Full Stack Tech Test

1. Add DB details: edit the `.env` file  
2. Run migrations: `php artisan migrate`  
3. Gen key: `php artisan key:generate`  
4. Compile assets: `npm install && npm run development`  
5. Run server: `php artisan serve`  
6. Go to: http://127.0.0.1:8000/

Note: the repo also contains the sql file `vacancies.sql`

ToDo:  
1. Move styling from js to scss