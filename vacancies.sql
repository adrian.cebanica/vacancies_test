-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 15, 2019 at 11:19 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_react`
--

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE `vacancies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`id`, `title`, `description`, `location`, `salary`, `created_at`, `updated_at`) VALUES
(2, 'Job Title', 'Lorem ipsum...', 'Barnett', '33,000 - 35,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(6, 'Job Title', 'Lorem ipsum...', 'Milton Keynes', '40,000 - 45,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(7, 'Job Title', 'Lorem ipsum...', 'Oxford', 'To be defined', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(8, 'Job Title', 'Lorem ipsum...', 'London', 'Based on experience', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(9, 'Job Title', 'Lorem ipsum...', 'Bath', '33,000 - 35,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(10, 'Job Title', 'Lorem ipsum...', 'Luton', '33,000 - 35,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(11, 'Job Title', 'Lorem ipsum...', 'Cambridge', '33,000 - 35,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(12, 'Job Title', 'Lorem ipsum...', 'Manchester', '33,000 - 35,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(13, 'Job Title', 'Lorem ipsum...', 'Birmingham', '33,000 - 35,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05'),
(14, 'Job Title', 'Lorem ipsum...', 'London', '55,000 - 85,000', '2019-11-15 07:44:05', '2019-11-15 07:44:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vacancies`
--
ALTER TABLE `vacancies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
