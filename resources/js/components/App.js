import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Vacancies from "./Vacancies";

export default class App extends Component {
    render() {
        return (
            <Vacancies />
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
