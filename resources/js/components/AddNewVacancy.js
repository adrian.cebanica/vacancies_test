import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {AppConfig} from './Config.js';
import {withStyles} from '@material-ui/core/styles';

const styles = theme => ({
    addButton: {
        backgroundColor: 'white',
        color: '#73c2b0',
        fontWeight: 'bold',
        "&:hover": {
            backgroundColor: "white"
        }
    },
    defaultBtn: {
        backgroundColor: '#73c2b0',
        fontWeight: 'bold',
        "&:hover": {
            backgroundColor: "#73c2b0"
        }
    }
});

class AddNewVacancy extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            title: '',
            description: '',
            location: '',
            salary: '',
        };
    }

    update = (name, e) => {
        this.setState({[name]: e.target.value});
    };

    openDialog = () => {
        this.setState({open: true});
    };

    closeDialog = () => {
        this.setState({
            open: false,
            title: '',
            description: '',
            location: '',
            salary: '',
        });
    };

    isFormValid = () => {
        let hasEmptyValues = false;
        for (var key in this.state) {
            if (!this.state.hasOwnProperty(key)) {
                continue;
            }

            if (this.state[key] === '') {
                hasEmptyValues = true;
                break;
            }
        }
        return !hasEmptyValues;
    };

    async onSubmit() {
        if (!this.isFormValid()) {
            return false;
        }
        console.log(this.state);

        const response = await fetch(AppConfig.API_BASE_URL + '/vacancies', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({
                "title": this.state.title,
                "description": this.state.description,
                "location": this.state.location,
                "salary": this.state.salary
            })
        });
        const vacancy = await response.json();
        window.location.reload(false);
    };

    render() {
        const {classes} = this.props;

        return (
            <div>
                <Button variant="contained" color="default" className={classes.addButton} onClick={this.openDialog.bind(this)}>Add New Vacancy</Button>
                <Dialog open={this.state.open} ref="dialogAddNew" aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add new</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Instructions...
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Vacancy Title"
                            fullWidth
                            error={this.state.title === ""}
                            helperText={this.state.title === "" ? 'Empty field!' : ' '}
                            value={this.state.title}
                            onChange={(e) => this.update("title", e)}/>

                        <TextField
                            margin="dense"
                            id="description"
                            label="Vacancy Description"
                            multiline rows="4"
                            fullWidth
                            error={this.state.description === ""}
                            helperText={this.state.description === "" ? 'Empty field!' : ' '}
                            value={this.state.description}
                            onChange={(e) => this.update("description", e)}/>

                        <TextField
                            margin="dense"
                            id="location"
                            label="Location"
                            placeholder="Milton Keynes"
                            fullWidth
                            error={this.state.location === ""}
                            helperText={this.state.location === "" ? 'Empty field!' : ' '}
                            value={this.state.location}
                            onChange={(e) => this.update("location", e)}/>

                        <TextField
                            margin="dense"
                            id="salary"
                            label="Salary"
                            placeholder="40,000 - 45,000"
                            fullWidth
                            error={this.state.salary === ""}
                            helperText={this.state.salary === "" ? 'Empty field!' : ' '}
                            value={this.state.salary}
                            onChange={(e) => this.update("salary", e)}/>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.closeDialog.bind(this)} variant="contained" color="default">
                            Cancel
                        </Button>
                        <Button onClick={this.onSubmit.bind(this)} variant="contained" color="primary" className={classes.defaultBtn}>
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(AddNewVacancy);