import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import {FaPoundSign} from 'react-icons/fa';
import {MdLocationOn} from 'react-icons/md';
import AddNewVacancy from "./AddNewVacancy";
import {AppConfig} from './Config.js';
import Pagination from "react-pagination-js";
import "react-pagination-js/dist/styles.css"; // import css

// todo: better naming
const styles = theme => ({
    appbar: {
        backgroundColor: '#668B9D',
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        padding: theme.spacing(1),
        marginBottom: '20px'
    },
    cardContent: {
        flexGrow: 1,
    },
    cardDetails: {
        margin: '5px 0'
    },
    contentHeader: {
        background: '#ebebeb',
        padding: '0 30px',
        height: '50px',
        marginBottom: '20px',
        alignItems: 'center',
        borderRadius: '3px'
    },
    icons: {
        marginTop: '3px',
        marginRight: '-30px',
        color: '#73c2b0'
    },
    detailsBtn: {
        fontWeight: 'bold',
    },
    defaultBtn: {
        fontWeight: 'bold',
        backgroundColor: '#73c2b0',
        "&:hover": {
            backgroundColor: "#73c2b0"
        }
    }
});

class VacanciesClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            originalList: null,
            vacancies: null,
            count: 0,
            isLoading: null,
            currentPage: 1
        };
    }

    changeCurrentPage = numPage => {
        this.setState({currentPage: numPage});
        //fetch a data
        //or update a query to get data
    };

    update = (name, e) => {
        this.setState({[name]: e.target.value});
    };

    componentDidMount() {
        this.getVacancies();
    }

    async getVacancies() {
        if (!this.state.vacancies) {
            try {
                this.setState({isLoading: true});
                const response = await fetch(AppConfig.API_BASE_URL + '/vacancies');
                const vacanciesList = await response.json();
                this.setState({originalList: vacanciesList.data, vacancies: vacanciesList.data, isLoading: false, count: vacanciesList.data.length});
            } catch (err) {
                this.setState({isLoading: false});
                console.error(err);
            }
        }
    }

    onFilter(el) {
        let state = this.state;
        let updatedList = this.state.originalList;
        updatedList = updatedList.filter(function (item) {
            return item.title.toLowerCase().search(state.filter.toLowerCase()) !== -1;
        });
        this.setState({vacancies: updatedList});
    }

    /**
     * TODO:
     *  - Move toolbar to file and call in App.js
     *  - Move sidebar to file and call in App.js
     * @returns {*}
     */
    render() {
        const {classes} = this.props;

        const itemsPerPage = 2;
        const totalSize = this.state.vacancies && this.state.vacancies.length;

        const indexOfLastItem = this.state.currentPage * itemsPerPage;
        const indexOfFirstItem = indexOfLastItem - itemsPerPage;
        const currentItems = this.state.vacancies && this.state.vacancies.slice(indexOfFirstItem, indexOfLastItem);

        const renderItems = currentItems && currentItems.map(card => (
            <Grid item key={card.id} xs={12}>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                        <Typography gutterBottom variant="h6" component="h2">
                            {card.title}
                        </Typography>
                        <Grid container className={classes.cardDetails}>
                            <Grid item xs={1} className={classes.icons}>
                                <MdLocationOn/>
                            </Grid>
                            <Grid item xs={3}>
                                <Typography>{card.location}</Typography>
                            </Grid>
                            <Grid item xs={1} className={classes.icons}>
                                <FaPoundSign/>
                            </Grid>
                            <Grid item xs={3}>
                                <Typography>{card.salary}</Typography>
                            </Grid>
                        </Grid>
                        <Typography>
                            {card.description}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button variant="contained" color="default" className={classes.detailsBtn}>
                            More Details
                        </Button>
                        <Button variant="contained" color="primary" className={classes.defaultBtn}>
                            Apply
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
        ));

        const pagination = (
            <div>
                <Pagination
                    currentPage={this.state.currentPage}
                    totalSize={totalSize}
                    sizePerPage={itemsPerPage}
                    changeCurrentPage={this.changeCurrentPage}
                    theme="bootstrap"
                />
            </div>
        );

        return (
            <React.Fragment>
                <CssBaseline/>
                <AppBar position="relative" className={classes.appbar}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            VACANCIES
                        </Typography>
                    </Toolbar>
                </AppBar>
                <main>
                    <Container className={classes.cardGrid} maxWidth="md">
                        <Grid container spacing={4}>
                            <Grid item xs={12} sm={3} md={3}>
                                <Typography gutterBottom variant="h6">
                                    Filter Vacancies
                                </Typography>
                                <Divider/>
                                <Typography>
                                    Keywords
                                </Typography>
                                <TextField
                                    id="filter-input"
                                    label="Job, Title, Keyword..."
                                    margin="dense"
                                    variant="outlined"
                                    fullWidth={true}
                                    onChange={(e) => this.update("filter", e)}
                                />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className={classes.defaultBtn}
                                    fullWidth={true}
                                    onClick={(e) => this.onFilter(e)}
                                >
                                    Filter Results
                                </Button>
                            </Grid>

                            <Grid item xs={12} sm={9} md={9}>
                                <Grid justify="space-between" container className={classes.contentHeader}>
                                    <Grid item>
                                        <Typography>
                                            Available Vacancies ({this.state.count})
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <div>
                                            <AddNewVacancy/>
                                        </div>
                                    </Grid>
                                </Grid>
                                {renderItems}

                                {pagination}
                            </Grid>
                        </Grid>
                    </Container>
                </main>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(VacanciesClass);