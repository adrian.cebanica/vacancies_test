<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vacancy;
use App\Http\Resources\Vacancy as VacancyResource;
use App\Http\Resources\VacancyCollection;

class VacancyController extends Controller
{
    public function index()
    {
        return new VacancyCollection(Vacancy::all());
    }

    public function show($id)
    {
        return new VacancyResource(Vacancy::findOrFail($id));
    }

    public function store(Request $request)
    {
//        $request->validate([
            //'name' => 'required|max:255',
//        ]);

        $vacancy = Vacancy::create($request->all());

        return (new VacancyResource($vacancy))
                ->response()
                ->setStatusCode(201);
    }

    public function delete($id)
    {
        $vacancy = Vacancy::findOrFail($id);
        $vacancy->delete();

        return response()->json(null, 204);
    }
}
